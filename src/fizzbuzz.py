"""
Fizzbuzz
"""


def fizzbuzz(i):
    """
    Print fizz or buzz of fizzbuzz
    :return:
    """

    if i % 15 == 0:
        i = 'FizzBuzz'
    elif i % 5 == 0:
        i = 'Buzz'
    elif i % 3 == 0:
        i = 'Fizz'
    return i


def main():
    n = int(input('Provide max range: '))
    fizbuzz(n)
