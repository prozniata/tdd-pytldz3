"""
hamming metric
"""


def hamming_metric(dna_c, dna_f):
    """
    Return hamming metric value
    :param dna_c:
    :param dna_f:
    :return:
    """

    metric_value = 0

    for c, f in zip(dna_c, dna_f):
        if c != f:
            metric_value += 1

    return metric_value


def i_am_your_father(fathers_list, child):
    """
    :param fathers_list: list of string
    :param child: str
    :return:
    """
    list_f = []
    for father in fathers_list:
        list_f.append(hamming_metric(father, child))

    if list_f.count(min(list_f)) > 1:
        raise ValueError('Somebody is cheating!!!')

    return list_f.index(min(list_f))


if __name__ == '__main__':
    pass
