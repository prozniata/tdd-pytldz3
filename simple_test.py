"""
Simple unittest
"""
import os
import unittest


class FirstTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUp')
        self.csv_file = [1000, 2000, 1001]

    def tearDown(self):
        print('tearDown')

    def test_first_test(self):
        print('first test')
        self.assertEqual([1000, 2000, 1001], self.csv_file)

        self.assertEqual(int(os.environ['COUNTRY_ID']), 1)

    def test_second_test(self):
        print('second test')
        self.assertNotIn(1, self.csv_file)


class SecondTestCase(unittest.TestCase):

    def test_from_second_test_case(self):
        print(self.__class__.__name__)
        self.assertNotIn(1, [3, 2])
