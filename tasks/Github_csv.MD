# Github CSV
Korzystając z przesłanego wcześniej przykładu pobierania danych z Github'a napisz program, który pobierze dane repozytoriów dla użytkowników z listy w pliku CSV.

### Zadanie
1. Na wejściu otrzymujemy plik CSV 
Imię, Nazwisko, Nazwa użytkownika na Github, Język programowania I, j. p. II

2. Generujemy plik w którym otrzymamy:
Imię, Nazwisko, Nazwa na Github, Nazwa Repozytorium, Tematy Repozytorium (po spacji), Liczba plików w języku x*, liczba pr, liczba gwiazdek

3. Piszemy z wykorzystaniem TDD.

### Kontakt:
Marcin Olejniczak olejniczakmarcin10@gmail.com

[![N|Solid](https://sdacademy.pl/wp-content/themes/sdacademy/img/logo.png)](https://nodesource.com/products/nsolid)
